*** Settings ***
Documentation    Collection Element
Library          SeleniumLibrary

*** Variables ***
${URL_BE}               http://beoffice-dev.flashcoffeetech.com/sign-in
${InputEmailTxt}        //body/div[@id='app']/div[1]/div[1]/div[1]/form[1]/div[3]/input[1]
${InputPasswordTxt}     //body/div[@id='app']/div[1]/div[1]/div[1]/form[1]/div[4]/input[1]
${ContinueBtn}          //button[@id='kt_sign_in_submit']
${RequestAccessBtn}     //a[contains(text(),'Create request access')]
${ServerListTxt}        //span[contains(text(),'Server List')]

*** Keywords ***
Navigate to BE Office
     Open Browser   url=${URL_BE}   browser=headlesschrome

Fill Email
     Input Text     ${InputEmailTxt}    admin@demo.com

Fill Password
     Input Password    ${InputPasswordTxt}    demo

Click Submit Button
     Click Element      ${ContinueBtn}

Make sure user navigate to dashboard
     Wait Until Page Contains Element    ${ServerListTxt}    25    error=Login Failed
