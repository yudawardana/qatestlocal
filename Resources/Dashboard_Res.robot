*** Settings ***
Documentation    Collection Element
Library          SeleniumLibrary

*** Variables ***
${URL_BE}               //span[contains(text(),'Dashboard')]
${InputEmailTxt}        //span[contains(text(),'Servers')]
${InputPasswordTxt}     //span[contains(text(),'Members')]
${ContinueBtn}          //span[contains(text(),'Requests')]
${RequestAccessBtn}     //body/div[@id='app']/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/input[1]
${ServerListTxt}        //span[contains(text(),'Server List')]

*** Keywords ***