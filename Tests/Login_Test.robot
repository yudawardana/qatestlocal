*** Settings ***
Documentation    Test Case for login BE Office
Library          SeleniumLibrary
Resource         ../Resources/Login_Res.robot
Test Teardown    Close All Browsers

*** Test Cases ***
QTA-TC-3 User can login to BE Office
    Navigate to BE Office
    Fill Email
    Fill Password
    Click Submit Button
    Make sure user navigate to dashboard



